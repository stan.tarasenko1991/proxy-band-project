// Modules
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Modal from 'react-bootstrap/Modal';

// Utils
import { POSTS_ROUTE } from '../utils/constants';
import { getUserAlbums, selectUser } from '../store/userStore/userSlice';

const UserInfo = (user) => {
  const { data } = user;
  const [show, setShow] = useState(false);
  const { albums } = useSelector((state) => state.user);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  function handleClose() {
    setShow(false);
  };  

  function handleShow() {
    dispatch(getUserAlbums(data.id));
    setShow(true);
  };
  
  function showPosts(data) {
    dispatch(selectUser(data));
    navigate(POSTS_ROUTE + '/' + (data.name).replace(/\s/g, '') + '/' + data.id);
  };

  return (
    <>
      <Card style={{ width: '100%' }}>
      <Card.Body>
        <Card.Title style={{ fontSize: '32px' }}>{data.name}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted" style={{ fontSize: '22px' }}>
          UserName: <span style={{ color: 'black' }}>{data.username}</span>
        </Card.Subtitle>
        <Card.Subtitle className="mb-2 text-muted" style={{ fontSize: '22px' }}>
          Phone: <span style={{ color: 'black' }}>{data.phone}</span>
        </Card.Subtitle>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Card.Link>{data.website}</Card.Link>
      </Card.Body>
      <Button 
        onClick={() => showPosts(data)}
        style={{ width: '200px', margin: '5px 20px' }} 
        variant="primary">
          Show Posts
      </Button>
      <Button 
        style={{ width: '200px', margin: '5px 20px 20px' }}
        onClick={() => handleShow()}
        variant="secondary">
          Show Albums
      </Button>
      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>{data.name} Albums</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {albums && albums.map((album) => 
            <li key={album.id} style={{ fontSize: '20px' }}>
              {album.title}
            </li>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Card>
    </>
  );
};

export default UserInfo;
