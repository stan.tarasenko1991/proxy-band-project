// Modules
import { Routes, Route } from 'react-router-dom';
import { projectRoutes } from '../routes';

// Componets
import MainPage from '../pages/MainPage';

const AppRouter = () => {
  return (
    <Routes>
      {projectRoutes.map(({ path, Component }) => 
        <Route key={path} path={path} element={Component} />
      )}
      <Route path="*" element={<MainPage />} />
    </Routes>
  );
};

export default AppRouter;
