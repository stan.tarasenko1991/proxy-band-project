// Components
import MainPage from './pages/MainPage/MainPage';
import PostsPage from './pages/PostsPage/PostsPage';

// Utils
import { MAIN_ROUTE, POSTS_ROUTE } from './utils/constants';

export const projectRoutes = [
  {
    path: MAIN_ROUTE,
    Component: <MainPage />
  },
  {
    path: POSTS_ROUTE + '/:name' + '/:id',
    Component: <PostsPage />
  }
];
