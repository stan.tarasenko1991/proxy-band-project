// Modules
import { configureStore } from '@reduxjs/toolkit';

// Slices
import userReducer from './userStore/userSlice';
import postReducer from './postStore/postSlice';

export const store = configureStore({
  reducer: {
    user: userReducer,
    post: postReducer,
  },
})
