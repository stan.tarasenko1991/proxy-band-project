// Modules
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getPostsById = createAsyncThunk(
  "postsList", 
  async (id) => {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/posts", {
          params: {
            userId: id
          }
        }
      );
      return response.data;
    } catch (error) {
      console.error(error);
    }
});

const postsSlice = createSlice({
  name: "post",
  initialState: {
    hasError: false,
    isLoading: false,
    posts: []
  },
  extraReducers: (builder) => {
    builder
      .addCase(getPostsById.pending, (state) => {
      state.isLoading = true;
      state.hasError = false;
    })
      .addCase(getPostsById.fulfilled, (state, action) => {
        state.posts = action.payload;
        state.isLoading = false;
        state.hasError = false
      })
      .addCase(getPostsById.rejected, (state) => {
        state.hasError = true
        state.isLoading = false;
      })
  }
});

export default postsSlice.reducer
