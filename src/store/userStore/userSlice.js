// Modules
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const getAllUsers = createAsyncThunk(
  "usersList", 
  async () => {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      return response.data;
    } catch (error) {
      console.error(error);
    }
});

export const getUserAlbums = createAsyncThunk(
  "albumsList", 
  async (id) => {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/albums", {
          params: {
            userId: id
          }
        }
      );
      return response.data;
    } catch (error) {
      console.error(error);
    }
});

const userSlice = createSlice({
  name: "user",
  initialState: {
    albums: [],
    hasError: false,
    isLoading: false,
    albumsHasError: false,
    albumsIsLoading: false,
    selectedUser: {},
    users: [],
  },
  reducers: {
    selectUser: (state, action) => {
      state.selectedUser = action.payload;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllUsers.pending, (state) => {
      state.isLoading = true;
      state.hasError = false;
    })
      .addCase(getAllUsers.fulfilled, (state, action) => {
        state.users = action.payload;
        state.isLoading = false;
        state.hasError = false
      })
      .addCase(getAllUsers.rejected, (state) => {
        state.hasError = true
        state.isLoading = false;
      })

      builder
      .addCase(getUserAlbums.pending, (state) => {
      state.albumsIsLoading = true;
      state.albumsHasError = false;
    })
      .addCase(getUserAlbums.fulfilled, (state, action) => {
        state.albums = action.payload;
        state.albumsIsLoading = false;
        state.albumsHasError = false
      })
      .addCase(getUserAlbums.rejected, (state) => {
        state.albumsHasError = true
        state.albumsIsLoading = false;
      })
  }
});

export const { selectUser } = userSlice.actions
export default userSlice.reducer
