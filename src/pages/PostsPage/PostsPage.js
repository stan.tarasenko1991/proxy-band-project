// Modules
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { MAIN_ROUTE } from '../../utils/constants';

// Store
import { getPostsById } from '../../store/postStore/postSlice';

// Styles
import './postPage-styles.css';

const PostsPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const { pathname } = location;
  const userId = /[^/]*$/.exec(pathname)[0];
  
  const { posts } = useSelector((state) => state.post);

  useEffect(() => {
    if (userId) {
      dispatch(getPostsById(userId));
    }
  }, [dispatch, userId]);
  
  return (
    <div className='mainPost'>
      <Card style={{ width: '100%', background: 'lightgrey' }}>
      <Card.Body>
        <Card.Title style={{ fontSize: '24px', margin: '15px 0' }}>Posts</Card.Title>
      </Card.Body>
      <ListGroup className="list-group-flush">
        {posts && posts.map((post) => 
          <div key={post.id}>
            <ListGroup.Item>
              <Card.Title>{post.title}</Card.Title>
              <div>{post.body}</div>
            </ListGroup.Item>
          </div>
        )}
      </ListGroup>
    </Card>
    <div className='mainPost_btn'>
    <Button 
        onClick={() => navigate(MAIN_ROUTE)}
        style={{ width: '200px', margin: '20px' }} 
        variant="primary">
          Back to Main
    </Button>
    </div>
    </div>
  );
};

export default PostsPage;
