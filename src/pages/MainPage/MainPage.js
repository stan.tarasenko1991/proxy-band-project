// Modules
import { useSelector } from 'react-redux';

// Components
import UserInfo from '../../components/UserInfo';

// Styles
import './mainPage-styles.css';

const MainPage = () => {
const data = useSelector((state) => state.user);
const { users } = data;

  return (
    <>
      {users 
        ? <div className='main'>
            {users.map((user) => 
              <div key={user.id} className='main_card'> 
                <UserInfo data={user} />
              </div>
            )}
          </div>
        : <div>Loading...</div>
      }
    </>
  );
};

export default MainPage;
